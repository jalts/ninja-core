/* 

1. Print odds 1-20
    Print out all odd numbers from 1 to 20
    The expected output will be 1, 3, 5, 7, 9, 11, 13, 15, 17, 19

2. Sum and Print 1-5
    Sum numbers from 1 to 5, printing out the current number and sum so far at each step of the way.
    The expected output will be: Num: 1, Sum: 1, Num: 2, Sum: 3, Num: 3, Sum: 6, Num: 4, Sum: 10, Num: 5, Sum: 15 
    
*/

// 1.

for(var i = 1; i < 20; i++){
    if(i % 2 !== 0){
        console.log(i)
    }
}



/* 

I need help on question 2 - just spent 90 minutes trying to solve

Garbage code below and running through past thoughts, I really need more algo practice so I can go back to the things I have already done... Essentially I need to memorize a little more - on question two I had to go to the internet and dig for answers after spending close to an hour on an answer.

var sum = 0;
x  = [1,2,3,4,5]
for (var [i]; x < 1000; x++)
{
    if (x % 3 === 0 || x % 5 === 0)
    {
       sum += x;
    }
}
console.log(sum);

var sum = x+num
var array = [1,2,3,4,5]
for(var x=1; x<=5; x++){
    if(x <= 5){
        console.log(x)
        x = sum
        console.log(x+sum)
    }
}




    while (x >= 4) {            // -- condition
        sum += number;        // -- body
        number++;             // -- updater
          }
          console.log(sum)
          console.log(number)




        if(array[x]>array[y])
        {
        var temp = array[y]
        array[y] = array[x];
        array[x] = temp;
        }
    }
}







var sum = 0;
var number = 1;
while (number <= 5) {  // -- condition
  sum += number;        // -- body
  number++;             // -- updater
}
console.log(sum)
console.log(number)

 */