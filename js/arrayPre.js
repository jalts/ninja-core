var arr = [8,6,7,5,3,0,9]
for(var i = 0; i < arr.length; i++){    
    console.log(arr[i]);
}

/* - Return
8 6 7 5 3 0 9  */


var arr = [7,3,8,4,2,0,1];
for(var i = 0; i < arr.length; i++){ 
    if(arr[i] % 2 == 0){
        console.log(arr[i]);
    } 
    else{
        console.log("That is odd!");
    }
}

/* - Return
That is Odd! 
That is Odd! 
8
4
2
0
That is Odd!  */


var arr = [1,3,8,-5,0,-2,4,-1];
var newArr = [];
for(var i = 0; i < arr.length; i++){
    if(arr[i] < 0){
        newArr.push(arr[i]);
        arr[i] = arr[i] * -1;
    }
    else if(arr[i] == 0){
        arr[i] = "Zero";
    }
    else{
        arr[i] = arr[i] * -1;
    }
}
console.log(arr);
console.log(newArr);

/* - Return

newArr = [-5,-2,-1]
arr = [-1,-3,-8,5,zero,2,-4,1]


Notes
arr.length = 7

-5
-2
-1
= [, 5, 2, 1] zero, = arr

1
3
8
0 = zero
4
= [,-1, -3, -8, zero, -4]

newArr = [-5,-2,-1]
arr = [-1,-3,-8,5,zero,2,-4,1]

*/