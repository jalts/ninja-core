cd ~
ls -a
cd desktop
ls
cd codingDojoWebFun
mkdir test
cd test
touch index.html
touch style.css
touch commands.txt
cd ..
mkdir desti
mv ~/desktop/codingDojoWebFun/test/index.html  ~/desktop/codingDojoWebFun/desti/index2.html
touch ~/desktop/codingDojoWebFun/test index.html - Forgot to copy orginal index file
cd test
ls
rm style.css
ls
cd ..
cd desti
ls
cd desti
rm index2.html
cd ..
rmdir desti
I've spent a fair amount of time on linux academy doing basic 101 stuff - also I've stood up a chef server and ran a few cookbook recipes through it so I'm 101 familiar with a CLI


Assignment II: Learning to Commit

I'm submitting this also for Assignment II - I don't like this assignment, I've been using git and uploading all assignments as GitLab links.  I did track some of your notes on commands and revert/reset however.


Git Commands!

git init - initialize the repository.
git add . - add all the files that were changed since the last back up to the staging area.
git status - shows you all the files that were changed since the last backup and which ones are already added to the staging area.
git commit -m "..." - commits the changes to the repository.
git checkout ____ - switches to the branch name provided in your git repository. This will create a new branch if the name provided doesn't exist.
git branch - shows all of your git branches and marks the one you are currently on.
git log - shows all the backups created in the repository.
git blame ____ - shows who wrote which line of code or in other words who is to be blamed for that particular line of code.
git remote add origin ____ - tells git to add a remote place called 'origin' to a remote URL ___.
git push - pushes the changes in your local repository to the remote repository.
git pull - pulls the changes in a remote repository to your own local repository.
git clone ___ - clones a remote repository in ___ to your own local folder.

Revert ie.

git revert -n HEAD
Finished one revert.
git revert -n 540ecb7 
Removed copy.txt 
Finished one revert.
git commit -m "revert 45eaf98 and 540ecb7"
Created commit 2b3c1de: revert 45eaf98 and 540ecb7
2 files changed, 0 insertions(+), 10 deletions(-)
delete mode 100644 copy.txt


Reset ie. - Warning!!! When we revert the changes using this command, the changes we revert are GONE FOREVER, ERASED FROM THE LOG, UNRECOVERABLE. This is what makes reset perfect for those situations when you want to UTTERLY AND COMPLETELY ERASE SOMETHING, but A TERRIBLE IDEA FOR ANYTHING ELSE

git reset 6c77676 --hard
OR
git reset <commit hash> <filename> --hard


Other information:

You can add as many remote repositories as you want by using git remote add <remote name>
You can also delete remote repository that you've added by using git remote remove <remote name>
To list all the remote repositories you are connected to, run git remote
To see more information about a remote repository, use git remote show <remote name>


git clone <URL of repository>
ie. git clone https://github.com/purpleshoelace/git-demo.git


Git ignore ie.

# ignores any file named "secret.txt"
secret.txt
# ignores any directory named "secrets"
secrets/
# ignores a file named "hidden.txt" located at the root of your working directory
/hidden.txt
# ignores a directory called "node_modules" located at the root of your working directory
/node_modules/
# ignores any file with a .png extension
*.png
# ignores any file or directory that begins in "cache", such as cache-file-01, cached_assets/, etc.
cache*
# ignores any file or directory that ends in "data", such as project_data/, big_file_of_data
*data


