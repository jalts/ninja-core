var testArr = [6,3,5,1,2,4]

var sum=0;
for(let i=0; i<testArr.length; i++){
    sum+=testArr[i];
    console.log('val:', testArr[i], 'sum:', sum);
}

/* return
val 6, Sum 6
val 3, Sum 9
val 5, Sum 14
val 1, Sum 15
val 2, Sum 17
val 4, Sum 21 */


var testArr = [6,3,5,1,2,4] 

for(let i=0; i<testArr.length; i++){
    testArr[i] = testArr[i] * i;
}
console.log(testArr);

/* return
0
3
10
3
8
20
*/

