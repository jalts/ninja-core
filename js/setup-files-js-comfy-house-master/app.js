//varibles

const cartBtn = document.querySelector('.cart-btn');
const closeCartBtn = document.querySelector('.close-cart');
const clearCartBtn = document.querySelector('.clear-btn');
const cartDOM = document.querySelector('.cart');
const cartOverlay = document.querySelector('.cart-overlay');
const cartItems = document.querySelector('.cart-item');
const cartTotal = document.querySelector('.cart-total');
const cartContent = document.querySelector('.cart-content');
const productsDOM = document.querySelector('.products-center');

// cart
let cart = []

// getting the products
class Products{
    async getProducts(){
            try {
                let result = await fetch('products.json');
                let data = await result.json();
                return data;
            } catch (error) {
                console.log(error);
                
            }
    }
}
// display products
class UI{}
// local storage
class Storage{}

document.addEventListener("DOMContentLoaded",()=>{
    const ui = new UI();
    const products = new Products();

    // get all products
    products.getProducts().then(data => console.log(data));
});



// stopping at 1:30 - https://www.youtube.com/watch?v=023Psne_-_4&list=PLWKjhJtqVAbleDe3_ZA8h3AO2rXar-q2V&index=21  