/* Stacks - https://www.youtube.com/watch?v=t2CEgPsws3U&list=PLWKjhJtqVAbleDe3_ZA8h3AO2rXar-q2V&index=3
- a good ie. is your browsers back button
- functions within a Stacks
    - push       - adding an element onto the top of the stack
    - pop        - removing the top element of a stack
    - peek       - checking the top element of a stack
    - length     - determining how many elements are on the stack
    
all arrays are stacks! ie. below

*/

var letters = [];
var word = 'racecar'
var rword = '';

// put letters of word into array
for(var i = 0; i < word.length; i++){
    letters.push(word[i]);
}

// pop off the array in reverse order
for(var i = 0; i < word.length; i++){
    rword += letters.pop();
}

if (rword === word){
    console.log(word + ' is a palindrome.');
}
else {
    console.log(word + ' is not a palindrome.');
}
