/* 
changing ccss with javascript
 */
<h2>What Can JavaScript Do?</h2>

<p id="demo">JavaScript can change the style of an HTML element.</p>

<button type="button" onclick="document.getElementByID('demo').style.fontSize=35px'">Click Me!</button>

/* 
changing text with javascript
*/

<h2>What Can JavaScript Do?</h2>

<p id="demo">JavaScript can change HTML content.</p>

<button type="button" onclick='document.getElementById("demo").innerHTML = "Hello JavaScript!"'>Click Me!</button>


/* 
turning a lightbulb off/on
*/

<h2>What Can JavaScript Do?</h2>

<p>JavaScript can change HTML attribute values.</p>

<p>In this case JavaScript changes the value of the src (source) attribute of an image.</p>

<botton onclick="document.getElementByID('myImage').src='pic_asdf.gif'">Turn on the light</botton>

<img id="myImage" src="pic_bulboff.gif" style="width:100px"></img>

<button onclick="document.getElementById('myImage').src='pic_bulboff.gif'">Turn off the light</button>


/* 
hide html elements
*/

<h2>What Can JavaScript Do?</h2>

<p id="demo">JavaScript can hide HTML elements.</p>

<button type="button" onclick="document.getElementById('demo').style.display='none'">Click Me!</button>


/* 
JS output

JavaScript Display Possibilities

JavaScript can "display" data in different ways:

Writing into an HTML element, using innerHTML.
Writing into the HTML output using document.write().
Writing into an alert box, using window.alert().
Writing into the browser console, using console.log().

*/

// using innerHTML

<h1>My First Web Page</h1>
<p>My First Paragraph</p>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = 5 + 6;
</script>

// using document.write()

<!DOCTYPE html>
<html>
<body>

<h1>My First Web Page</h1>
<p>My first paragraph.</p>

<script>
document.write(5 + 6);
</script>

</body>
</html>



/* 

skiping to Learn JavaScript - Full Course for Beginners - https://www.youtube.com/watch?v=PkZNo7MFNFg&list=PLWKjhJtqVAbleDe3_ZA8h3AO2rXar-q2V

*/

/* 

data types:
- undefined
    - hasnt been defined
- null
    - means nothing
- boolean
    - true or false
- string
    - more or less a word
- symbol
    - an immutable primitive value that is unique
- number
    - a number
- object
    - can store many key value pairs

there are 3 ways to declare a var in JS
*/
- var
    ie. var myName = "james"
    // will be used throughout your entire program
- let
    ie. let ourName = 'altheide'
    // will only be used within the scope of the delcared program
- const
    ie. const pi = 3.14
    // should never change, it can never change


/* 
declaring and assigning
*/

// declaring
var studyCapVar;
var properCamelCase;
var vegetaOver9000;

// assignments
studyCapVar = 10;
properCamelCase = "A string";
vegetaOver9000 = 9001;


/* appending varibles values - you can do this with any mathmatical operator - you can also do this with strings!*/

var a = 3;
var b = 17;

a += 12; // we are adding 3+12 here = a
b += 7; // 17 + 7 = be

/* If you're trying to add a char however formatting is getting in the way, do this, be it a " or ' mark - refer to this

CODE    OUTPUT
\'      single quote
\"      double quote
\\      backslash
\n      newline
\r      carriage return
\t      tab
\b      backspace
\f      form feed

ie.

var myStr = "firstLine\n\t\\secondLine\nthirdLine" - if you were to log out all of this, you would see three different lines

*/

/* 

https://www.youtube.com/watch?v=PkZNo7MFNFg&list=PLWKjhJtqVAbleDe3_ZA8h3AO2rXar-q2V
stopping at 1:17 in - I need to look at building an actual webapp, I'm ready. 

*/


