""" Returning a simple value """
def get_formatted_name(first_name, last_name):
    full_name = f"{first_name} {last_name}"
    return full_name.title()

musician = get_formatted_name('jimi', 'hendrix')
print(musician)

""" end """

""" making an argument optional p.139"""
def get_formatted_name(first_name, last_name, middle_name=''):
    if middle_name:
        full_name = f"{first_name} {middle_name} {last_name}"
    else
        full_name = f"{first_name} {last_name}"

musician = get_formatted_name('jimi', 'hendrix')
print(musician)

musician = get_formatted_name('john', 'hooker', 'lee')
print(musician)

""" Returning a Dictionary """
def build_person(first_name, last_name):
    person = {'first': first_name, 'last': last_name}
    return person

musician = build_person('jimi', 'hendrix')
print(musician)

""" adding a special value in your dictionary as optional """
def build_person(first_name, last_name, age=None):
    if age:
        person['age'] = age

musician = build_person('jimi', 'hendrix', age=27)
print(musician)

""" end """

""" using a function with a while loop """


