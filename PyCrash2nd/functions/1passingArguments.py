""" simple function that greets a user  """
def greet_user():
    print('hello!')

greet_user()

""" end """

""" passing information to a function """
def greet_user(username):
    print(f"Hello, {username.title()}!")

greet_user('jesse')

""" end """

""" positional arguments """
def describe_pet(animal_type, pet_name):
    print(f'\nI have a {animal_type}/')
    print(f"My {animal_type}'s name is {pet_name.title()}.'")

describe_pet('hamster', 'harry')

""" end """

""" Keyword Arguments """
def describe_pet(animal_type, pet_name):
    print(f'\nI have a {animal_type}/')
    print(f"My {animal_type}'s name is {pet_name.title()}.'")

describe_pet(animal_type='hamster', pet_name='harry')

""" end """

""" Default values """
def describe_pet(animal_type, pet_name='dog'):
    print(f'\nI have a {animal_type}/')
    print(f"My {animal_type}'s name is {pet_name.title()}.'")

describe_pet(pet_name='willie')
""" "better" """ describe_pet(pet_name='willie')
""" You're still going to get the dog stuff, as the arguments default value is dog when not givin on the parameter """

""" You could also do this, assuming that you input a non default value """

def describe_pet(animal_type, pet_name='dog'):
    print(f'\nI have a {animal_type}/')
    print(f"My {animal_type}'s name is {pet_name.title()}.'")

describe_pet(pet_name='willie' animal_type='hamster')

""" note - you need to list default values after real values -  this allows py to continue tinterpereting positional; arguments correctly """
""" end """












